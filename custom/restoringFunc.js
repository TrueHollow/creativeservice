require('../service/dotEnv_proxy');
const fs = require('fs');
const path = require('path');
const deepEqual = require('deep-equal');
const parse = require('csv-parse/lib/sync');
const Logiforms = require('../service/Logiforms');
const IO = require('../service/IO');
const logger = require('../logger')('custom/restoringFunc.js');

const getObjects = async () => {
  const tempDirectory = path.resolve(__dirname, '..', 'temp');
  const files = fs.readdirSync(tempDirectory);
  const result = [];
  files.forEach(file => {
    const fullPath = path.resolve(tempDirectory, file);
    logger.debug(`Parsing file: ${fullPath}`);
    const rawContent = fs.readFileSync(fullPath, { encoding: 'utf8' });
    const csvContent = parse(rawContent, {
      columns: true,
      skip_empty_lines: true,
    });
    result.push(...csvContent);
  });
  logger.debug(`Total found: ${result.length} items`);
  return result;
};

/**
 * Remove duplicates with deep check
 * @param {Array<Object>} items
 * @return {[]}
 */
const removeDuplicates = items => {
  const result = [];
  while (items.length) {
    const [item] = items.splice(0, 1);
    let hasDuplicate = false;
    // eslint-disable-next-line no-restricted-syntax
    for (const value of items) {
      hasDuplicate = deepEqual(item, value, { strict: true });
      if (hasDuplicate) {
        break;
      }
    }
    if (hasDuplicate) {
      logger.debug(`Skipping item (has duplicate): ${JSON.stringify(item)}`);
    } else {
      result.push(item);
    }
  }
  logger.debug(`After removing duplicates: ${result.length} items`);
  return result;
};

/**
 * Filtration function
 * @param {Object} item
 * @return {boolean}
 */
const filtration = item => {
  return !item.CASHRECEIVED;
};

const dateF = new Date(Date.parse('2019-11-01T00:00:00.000Z'));
const filtrationByDateSubmitted = item => {
  if (typeof item.datesubmitted !== 'string') {
    throw new Error('Invalid field type');
  }
  const dateValue = new Date(Date.parse(item.datesubmitted));
  return dateValue > dateF;
};

/**
 * Saving temp result
 * @param {Array<Object>} dataArray
 * @return {Promise<void>}
 */
const savingTemproraryResult = async dataArray => {
  const data = await IO.GetBufferResult(dataArray);
  logger.debug('Saving result into restore.csv');
  fs.writeFileSync(path.resolve(__dirname, '..', 'restore.csv'), data);
};

const validation = item => {
  const result = {};
  Object.entries(item).forEach(([key, value]) => {
    if (value === '') {
      logger.debug(`Skipping key: ${key} (empty string)`);
    } else {
      result[key] = value;
    }
  });
  return result;
};

const main = async () => {
  const items = await getObjects();
  const filteredItems = items
    .filter(filtration)
    .filter(filtrationByDateSubmitted)
    .filter(item => item.BILLFIRST !== 'James Black');
  logger.debug(`After filtration: ${filteredItems.length} items`);
  const uniqueItems = removeDuplicates(filteredItems);
  await savingTemproraryResult(uniqueItems);
  const form = { Name: 'Order Form 3.0' };
  const { Name } = form;
  logger.info('Start processing form: %s', Name);
  const logiforms = new Logiforms(form);
  const formId = await logiforms.getFormId();
  if (!formId) {
    logger.error('Form id not received!');
    return;
  }
  logger.info(`Saving bulk data into form (${formId})`);
  await logiforms.createDataBulk(formId, uniqueItems.map(validation));
};

main().then(() => {
  logger.info('Script finished');
});
