require('../service/dotEnv_proxy');
const Logiforms = require('../service/Logiforms');
const logger = require('../logger')('custom/updateForms.js');
const { delay } = require('../service/Net');

logger.info('Script started');

const MAX_REQUESTS = 50;
const WAIT_INTERVAL = 60 * 1000;

/**
 * Universal update Record function
 * @param {Number} formId
 * @param {Array<Object>} records
 * @param {Function} updateFunc
 * @return {Promise<void>}
 */
async function updateRecords(formId, records, updateFunc) {
  if (records.length === 0) {
    logger.debug('No records for update.');
    return;
  }
  const iterationArray = [];
  while (records.length) {
    const iterationRecords = records.splice(0, MAX_REQUESTS);
    iterationArray.push(iterationRecords);
  }
  logger.info(`Will be ${iterationArray.length} iterations`);
  let itId = 0;
  // eslint-disable-next-line no-restricted-syntax
  for (const recordsIteration of iterationArray) {
    itId += 1;
    logger.debug(`Waiting for api ready: ${WAIT_INTERVAL}`);
    logger.info(`Iteration #${itId} is running`);
    // eslint-disable-next-line no-await-in-loop
    await delay(WAIT_INTERVAL);
    const promises = recordsIteration.map(updateFunc);
    // eslint-disable-next-line no-await-in-loop
    await Promise.all(promises);
  }
}

async function updateHHESPaid(logiforms, formId) {
  const fieldList = ['RecordID', 'CASHRECEIVED', 'HHESPAID'];
  const recordsIds = await logiforms.getIdsWitFilter(formId, fieldList);
  logger.info(`Records count: ${recordsIds.length}`);

  const filteredRecords = recordsIds.filter(record => {
    return !!(record.CASHRECEIVED && !record.HHESPAID);
  });
  logger.info(`After filtration: ${filteredRecords.length}`);

  await updateRecords(formId, filteredRecords, async record => {
    const { RecordID: recordId } = record;
    // eslint-disable-next-line no-await-in-loop
    const res = await logiforms.updateRecord(formId, recordId, {
      HHESPAID: true,
    });
    logger.debug(res);
  });
}

async function fixCashReceived(logiforms, formId) {
  const fieldList = ['RecordID', 'CASHRECEIVED'];
  const recordsIds = await logiforms.getIdsWitFilter(formId, fieldList);
  logger.info(`Records count: ${recordsIds.length}`);

  const filteredRecords = recordsIds.filter(record => {
    return typeof record.CASHRECEIVED !== 'boolean';
  });
  logger.info(`After filtration: ${filteredRecords.length}`);

  await updateRecords(formId, filteredRecords, async record => {
    const { RecordID: recordId } = record;
    // eslint-disable-next-line no-await-in-loop
    const res = await logiforms.updateRecord(formId, recordId, {
      CASHRECEIVED: false,
    });
    logger.debug(res);
  });
}

async function setShipTypeTotal(logiforms, formId) {
  const fieldList = [
    'RecordID',
    'SHIPTYPEA',
    'SHIPTYPE1',
    'SHIPTYPE2',
    'SHIPTYPE3',
    'SHIPTYPE4',
    'SHIPTYPE5',
    'SHIPTYPE6',
    'SHIPTYPE7',
    'SHIPTYPE8',
    'SHIPTYPE9',
    'SHIPTYPE10',
    `ShipTypeTotal`, // To check for already processed
  ];
  const recordsIds = await logiforms.getIdsWitFilter(formId, fieldList);
  logger.info(`Records count: ${recordsIds.length}`);

  const filteredRecords = recordsIds.filter(record => {
    return !record.ShipTypeTotal;
  });
  logger.info(`After filtration: ${filteredRecords.length}`);

  await updateRecords(formId, filteredRecords, async record => {
    const {
      RecordID: recordId,
      SHIPTYPEA,
      SHIPTYPE1,
      SHIPTYPE2,
      SHIPTYPE3,
      SHIPTYPE4,
      SHIPTYPE5,
      SHIPTYPE6,
      SHIPTYPE7,
      SHIPTYPE8,
      SHIPTYPE9,
      SHIPTYPE10,
    } = record;
    const template = `${SHIPTYPEA} ${SHIPTYPE1} ${SHIPTYPE2} ${SHIPTYPE3} ${SHIPTYPE4} ${SHIPTYPE5} ${SHIPTYPE6} ${SHIPTYPE7} ${SHIPTYPE8} ${SHIPTYPE9} ${SHIPTYPE10}`;
    // eslint-disable-next-line no-await-in-loop
    const res = await logiforms.updateRecord(formId, recordId, {
      ShipTypeTotal: template,
    });
    logger.debug(res);
  });
}

const main = async () => {
  const form = { Name: 'Order Form 3.0' };
  const { Name } = form;
  logger.info('Start processing form: %s', Name);
  const logiforms = new Logiforms(form);
  const formId = await logiforms.getFormId();
  if (!formId) {
    logger.error('Form id not received!');
    return;
  }
  logger.info(`Getting ids for form (${formId})`);
  await fixCashReceived(logiforms, formId);
  await updateHHESPaid(logiforms, formId);
  await setShipTypeTotal(logiforms, formId);
};

main().then(() => {
  logger.info('Script finished');
});
