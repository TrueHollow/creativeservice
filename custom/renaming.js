const parse = require('csv-parse/lib/sync');
require('../service/dotEnv_proxy');
const DOStrorageHandler = require('../service/DOStrorageHandler');
const logger = require('../logger')('custom/renaming.js');
const { delay } = require('../service/Net');

const s3 = new DOStrorageHandler();

const reReceipt = /receipt-\d+\.zip/gi;
const reCsv = /Book Orders-\d+\.csv/gi;

/**
 * Process values of map
 * @param <Array<Object>> arr
 * @return {Promise<void>}
 */
const processGroup = async ([, arr]) => {
  const csv = arr.find(s3Object => {
    const { Key } = s3Object;
    return reCsv.test(Key);
  });
  if (!csv) {
    return;
  }
  const { Key } = csv;
  const csvFile = await s3.getFile({
    Key,
  });
  const { Body } = csvFile;
  const csvString = Body.toString('utf8');
  const records = parse(csvString, {
    delimiter: ',',
  });
  const [headers] = records;
  const RecordIDIndex = headers.indexOf('RecordID');
  const INVOICEIndex = headers.indexOf('INVOICE');
  const BILLLASTIndex = headers.indexOf('BILLLAST');
  if (RecordIDIndex === -1) {
    logger.warn('RecordID -1');
  }
  for (let i = 1; i < records.length; i += 1) {
    const record = records[i];
    const RecordID = record[RecordIDIndex];
    const INVOICE = record[INVOICEIndex];
    const BILLLAST = record[BILLLASTIndex];
    const s3File = arr.find(s3Object => {
      const { Key: k } = s3Object;
      // 20190929/Order Form 3.0_recepts/receipt-1000.zip
      return k.indexOf(`/receipt-${RecordID}.zip`) !== -1;
    });
    if (s3File) {
      const { Key: k } = s3File;
      const parts = k.split('/');
      parts[parts.length - 1] = `invoice-${INVOICE}-${BILLLAST}.zip`;
      const DestKey = parts.join('/');
      // eslint-disable-next-line no-await-in-loop
      await s3.copyFile({ CopySource: k, Key: DestKey });
      // eslint-disable-next-line no-await-in-loop
      await s3.deleteFile({ Key: k });
      // eslint-disable-next-line no-await-in-loop
      await delay(3000);
    }
  }
};

const main = async () => {
  logger.debug('Script started');
  await s3.init();
  const list = await s3.getList();
  const { Contents } = list;
  const filtratedContents = Contents.filter(s3Object => {
    const { Key } = s3Object;
    if (reReceipt.test(Key)) {
      return true;
    }
    if (reCsv.test(Key)) {
      return true;
    }
    return false;
  });
  const map = new Map();
  filtratedContents.forEach(s3Object => {
    const { Key } = s3Object;
    const [mapKey] = Key.split('/');
    if (map.has(mapKey)) {
      const arr = map.get(mapKey);
      arr.push(s3Object);
    } else {
      const arr = [s3Object];
      map.set(mapKey, arr);
    }
  });
  // eslint-disable-next-line no-restricted-syntax
  for (const group of map) {
    // eslint-disable-next-line no-await-in-loop
    await processGroup(group);
  }
};

main().catch(e => logger.error(e));
