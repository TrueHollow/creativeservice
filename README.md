#  Creative Service

## System requirements:

Node.js version 10 or higher. 

## Prepare

1. Install dependencies.
`npm i --production`

2. [Get service account credentials](https://github.com/HuasoFoundries/node-google-drive#readme) and save it to
config directory with name `credentials.json`

## Run

`npm start`

## Support environment files with [`dotenv`](https://www.npmjs.com/package/dotenv#rules)

You can use a `.env` file to set environment variables:
```dotenv
LOGIFORMS_API_KEY=
# Monday 12 AP
CRON_SCHEDULE=0 0 0 * * 1
DELETE_ORDERS=false

DO_REGION=sfo2
DO_ACCESS_KEY=
DO_SECRET_KEY=
DO_BUCKET=logiformsstorage
```
