const path = require('path');
const fs = require('fs');
const NodeGoogleDrive = require('node-google-drive');
const dateformat = require('dateformat');
const IO = require('./IO');
const config = require('../config');
const logger = require('../logger')('service/GoogleDrive.js');

const RootFolder =
  process.env.GOOGLE_DRIVE_ROOT || config.GoogleDrive.Root_Folder;
const CREDENTIALS_FILENAME = 'credentials.json';
const PATH_TO_CREDENTIALS = path.resolve(
  __dirname,
  '..',
  'config',
  CREDENTIALS_FILENAME
);

let credsServiceUser;
try {
  const str = fs.readFileSync(PATH_TO_CREDENTIALS);
  credsServiceUser = JSON.parse(str);
} catch (e) {
  logger.error('Cannot load service credentials! (%s)', JSON.stringify(e));
}

class GoogleDrive {
  static async Upload() {
    logger.info('Creating Google Drive instance.');
    const googleDriveInstance = new NodeGoogleDrive({
      ROOT_FOLDER: 'root',
    });

    logger.info('Google Drive: Auth.');
    await googleDriveInstance.useServiceAccountAuth(credsServiceUser);
    logger.info('Google Drive: start file uploading.');
    const dateStr = dateformat(new Date(), 'yyyymmdd');
    const outputFileName = `output-${dateStr}.csv`;
    logger.info(`Output filename: ${outputFileName}`);
    await googleDriveInstance.writeFile(
      IO.OutputFilePath,
      RootFolder,
      outputFileName,
      'text/csv'
    );
    logger.info('Google Drive: uploading is finished.');
  }
}

module.exports = GoogleDrive;
