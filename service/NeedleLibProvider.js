const needle = require('needle');

needle.defaults({
  open_timeout: 60000,
  response_timeout: 3000,
});

const { LOGIFORMS_API_KEY } = process.env;

const options = {
  compressed: true,
  json: true,
  username: 'apikey',
  password: LOGIFORMS_API_KEY,
};

/**
 * Get json(gzip) result by url
 * @param {string} url
 * @return {Promise<Object>}
 */
const GetRequest = async url => {
  const response = await needle('GET', url, null, options);
  return { statusCode: response.statusCode, json: response.body };
};

/**
 * Perform POST request by url and send data
 * @param {string} url
 * @param {Object} data
 * @return {Promise<Object>}
 */
const PostRequest = async (url, data) => {
  const response = await needle('GET', url, data, options);
  return { statusCode: response.statusCode, json: response.body };
};

/**
 * Perform DELETE request by url
 * @param {string} url
 * @return {Promise<Object>}
 */
const DeleteRequest = async url => {
  const response = await needle('DELETE', url, null, options);
  return { statusCode: response.statusCode, json: response.body };
};

/**
 * Download buffer by url
 * @param {string} url
 * @return {Promise<{ statusCode, data }>}
 * todo: Need to test this.
 */
const GetRequestBinary = async url => {
  const binaryOptions = {
    ...options,
    json: false,
    response_timeout: 60000,
  };
  const response = await needle('GET', url, null, binaryOptions);
  return { statusCode: response.statusCode, data: response.body };
};

module.exports = {
  GetRequest,
  DeleteRequest,
  GetRequestBinary,
  PostRequest,
};
