const path = require('path');
const stringify = require('csv-stringify');
const logger = require('../logger')('service/IO.js');

const OUTPUT_DIRECTORY = path.resolve(__dirname, '..', 'output');
const OUTPUT_FILEPATH = path.resolve(OUTPUT_DIRECTORY, 'output.csv');

function isIterable(obj) {
  // checks for null and undefined
  if (obj == null) {
    return false;
  }
  return typeof obj[Symbol.iterator] === 'function';
}

/**
 * Convert array of objects to csv string.
 * @param {Array<Object>} arr Of objects
 * @return {Promise<string>}
 */
const getCSVString = async arr => {
  return new Promise((resolve, reject) => {
    const options = {
      delimiter: ',',
      header: true,
    };
    stringify(arr, options, (err, records) => {
      if (err) {
        return reject(err);
      }
      return resolve(records);
    });
  });
};

class IO {
  /**
   * Get buffer from array of data
   * @param {Array<Object>} data
   * @return {Promise<Buffer>}
   * @constructor
   */
  static async GetBufferResult(data) {
    if (!isIterable(data)) {
      throw new Error('Data must be iterable!');
    }
    logger.debug('Converting data to csv');
    const str = await getCSVString(data);
    logger.debug('Get Buffer');
    return Buffer.from(str, 'utf8');
  }

  static get OutputFilePath() {
    return OUTPUT_FILEPATH;
  }
}

module.exports = IO;
