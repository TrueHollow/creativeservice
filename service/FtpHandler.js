const PromiseFtp = require('p-ftp');
const dateformat = require('dateformat');
const logger = require('../logger')('service/FtpHandler.js');
const config = require('../config');

const { FTP_LOGIN, FTP_PASSWORD, FTP_HOST } = process.env;

class FtpHandler {
  constructor() {
    this.ftp = new PromiseFtp();
    this.isReady = false;
  }

  async init() {
    if (this.isReady) {
      return;
    }
    this.isReady = true;
    logger.info('');
    const credentials = {
      host: FTP_HOST,
      user: FTP_LOGIN,
      password: FTP_PASSWORD,
    };
    const serverMessage = await this.ftp.connect(credentials);
    logger.info(`Server response: ${serverMessage}`);
    const response = await this.ftp.cwd(config.Ftp.Root_Folder);
    if (response) {
      logger.info('CWD: %s', JSON.stringify(response));
    }
    const dateStr = dateformat(new Date(), 'yyyymmdd');
    await this.ftp.mkdir(dateStr, true);
    await this.ftp.cwd(`${config.Ftp.Root_Folder}/${dateStr}`);
  }

  async sendFile({ data, outputFilename }) {
    logger.debug('Uploading file %s', outputFilename);
    return this.ftp.put(data, outputFilename);
  }

  async finish() {
    if (this.isReady) {
      await this.ftp.end();
    }
  }
}

module.exports = FtpHandler;
