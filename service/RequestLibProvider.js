const request = require('request');

request.defaults({ pool: { maxSockets: Infinity }, timeout: 3000 });

const { LOGIFORMS_API_KEY } = process.env;

/**
 * Download buffer by url
 * @param {string} url
 * @return {Promise<{ statusCode, data }>}
 */
const GetRequestBinary = async url => {
  return new Promise((resolve, reject) => {
    request(
      {
        url,
        method: 'GET',
        auth: {
          user: 'apikey',
          pass: LOGIFORMS_API_KEY,
        },
        encoding: null,
        timeout: 60000,
      },
      (err, iM, data) => {
        if (err) {
          return reject(err);
        }
        return resolve({ statusCode: iM.statusCode, data });
      }
    );
  });
};

/**
 * Get json(gzip) result by url
 * @param {string} url
 * @return {Promise<Object>}
 */
const GetRequest = async url => {
  return new Promise((resolve, reject) => {
    request(
      {
        url,
        method: 'GET',
        gzip: true,
        json: true,
        auth: {
          user: 'apikey',
          pass: LOGIFORMS_API_KEY,
        },
      },
      (err, iM, json) => {
        if (err) {
          return reject(err);
        }
        return resolve({ statusCode: iM.statusCode, json });
      }
    );
  });
};

/**
 * Perform DELETE request by url
 * @param {string} url
 * @return {Promise<Object>}
 */
const DeleteRequest = async url => {
  return new Promise((resolve, reject) => {
    request(
      {
        url,
        method: 'DELETE',
        gzip: true,
        json: true,
        auth: {
          user: 'apikey',
          pass: LOGIFORMS_API_KEY,
        },
      },
      (err, iM, json) => {
        if (err) {
          return reject(err);
        }
        return resolve({ statusCode: iM.statusCode, json });
      }
    );
  });
};

/**
 * Perform POST request by url and send data
 * @param {string} url
 * @param {Object} data
 * @return {Promise<Object>}
 */
const PostRequest = async (url, data) => {
  return new Promise((resolve, reject) => {
    request(
      {
        url,
        method: 'POST',
        gzip: true,
        json: true,
        auth: {
          user: 'apikey',
          pass: LOGIFORMS_API_KEY,
        },
        body: data,
      },
      (err, iM, json) => {
        if (err) {
          return reject(err);
        }
        return resolve({ statusCode: iM.statusCode, json });
      }
    );
  });
};

module.exports = {
  GetRequestBinary,
  GetRequest,
  DeleteRequest,
  PostRequest,
};
