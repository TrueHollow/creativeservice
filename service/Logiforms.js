const Bottleneck = require('bottleneck/es5');
const { Net } = require('./Net');
const logger = require('../logger')('service/Logiforms.js');
const config = require('../config');

const GET_ALL_FORMS = 'https://forms.logiforms.com/api/1.0/form/';

const HTTP_OK = 200;

const PAST_DEFAULT = 1000 * 60 * 60 * 24 * 30 * 6; // 6 months

const DEFAULT_DELETE_ORDERS = false;
let DELETE_ORDERS = process.env.DELETE_ORDERS || DEFAULT_DELETE_ORDERS;
if (typeof DELETE_ORDERS === 'string') {
  const str = DELETE_ORDERS.toLowerCase();
  switch (str) {
    case '1':
    case 'true':
      DELETE_ORDERS = true;
      break;
    case '0':
    case 'false':
      DELETE_ORDERS = false;
      break;
    default:
      DELETE_ORDERS = DEFAULT_DELETE_ORDERS;
  }
}

class Logiforms {
  constructor(form) {
    this.form = form;
    this.limiter = new Bottleneck(config.Logiforms.Limiter);
    this.GetJson = this.limiter.wrap(Net.GetJson);
    this.GetBinary = this.limiter.wrap(Net.GetBinary);
    this.PostJson = this.limiter.wrap(Net.PostJson);
    this.DeleteMethod = this.limiter.wrap(Net.DeleteMethod);
  }

  async getFormId() {
    const { json, statusCode } = await this.GetJson(GET_ALL_FORMS);
    if (!json.success || statusCode !== HTTP_OK) {
      logger.error(json.error);
      return null;
    }
    const { Name } = this.form;
    const formObject = json.data.forms.find(form => form.name === Name);
    if (!formObject) {
      logger.warn(`Form with name ${Name} not found`);
      return null;
    }
    return formObject.id;
  }

  async getAttachmentsFor(formId, id) {
    const url = `https://forms.logiforms.com/api/1.0/form/${formId}/data/attachments?startID=${id}&endID=${id}`;
    const { data, statusCode } = await this.GetBinary(url);
    if (statusCode !== HTTP_OK) {
      logger.warn(`Form: ${formId}, order ${id}, ${statusCode}`);
      return null;
    }
    return data;
  }

  async deleteSortedRecords(formId, ids) {
    const minId = Math.min(...ids);
    const maxId = Math.max(...ids);
    if (!Number.isInteger(minId) || !Number.isInteger(maxId)) {
      logger.warn(`Min or Max is not valid (${minId} or ${maxId})`);
      return;
    }
    const url = `https://forms.logiforms.com/api/1.0/form/${formId}/data/delete/`;
    const query = {
      startID: minId,
      endID: maxId,
      commit: DELETE_ORDERS,
    };
    const { json, statusCode } = await this.PostJson(url, query);
    if (!json.success || statusCode !== HTTP_OK) {
      logger.error('Error deleting: %s', json.error);
      return;
    }
    const { data } = json;
    const { rowsPendingDelete, rowsdeleted } = data;
    if (rowsdeleted) {
      logger.debug('Deleted: %s orders', rowsdeleted);
    } else if (rowsPendingDelete) {
      logger.debug('Can be deleted: %s orders', rowsPendingDelete);
    } else {
      logger.warn('Unknown data: %s', JSON.stringify(data));
    }
  }

  /**
   * Update record
   * @param {number} formId
   * @param {number} recordId
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  async updateRecord(formId, recordId, data) {
    const URL = `https://forms.logiforms.com/api/1.0/form/${formId}/data/${recordId}`;
    return this.PostJson(URL, data);
  }

  /**
   * Get records Ids using filter
   * @param {Number} formId
   * @param {Array<string>} [fieldList=['RecordID']]
   * @param {Array<{ Name, Value}>} [filter=[]]
   * @returns {Promise<Array<{}>>}
   */
  async getIdsWitFilter(formId, fieldList = ['RecordID'], filter = []) {
    const query = [];
    if (fieldList.length) {
      const fields = `fieldlist=${fieldList.join(',')}`;
      query.push(fields);
    }
    if (filter.length) {
      const filterQuery = filter.map(q => `${q.Name}=${q.Value}`).join('&');
      query.push(filterQuery);
    }
    const queryStr = query.join('&');
    const INITIAL_URL = `https://forms.logiforms.com/api/1.0/form/${formId}/data?${queryStr}`;
    let page = 1;
    const ids = [];
    let json;
    do {
      const url = `${INITIAL_URL}&page=${page}`;
      // eslint-disable-next-line no-await-in-loop
      const result = await this.GetJson(url);
      const { statusCode } = result;
      json = result.json;
      if (!json.success || statusCode !== HTTP_OK) {
        logger.error(json.error);
        return null;
      }
      json.data.records.forEach(item => ids.push(item));
      page += 1;
    } while (json.data.current_page < json.data.total_pages);
    return ids;
  }

  async getIds(formId) {
    const Past = this.form.Past || PAST_DEFAULT;
    const startDate = new Date(new Date().getTime() - Past)
      .toJSON()
      .substr(0, 19);
    const INITIAL_URL = `https://forms.logiforms.com/api/1.0/form/${formId}/data?fieldlist=RecordID&endDate=${startDate}`;
    const ids = [];
    let page = 1;
    let json; // Because I get all forms at this moment
    do {
      const url = `${INITIAL_URL}&page=${page}`;
      // eslint-disable-next-line no-await-in-loop
      const result = await this.GetJson(url);
      const { statusCode } = result;
      json = result.json;
      if (!json.success || statusCode !== HTTP_OK) {
        logger.error(json.error);
        return null;
      }
      json.data.records.forEach(item => ids.push(item.RecordID));
      page += 1;
    } while (json.data.current_page < json.data.total_pages);
    return ids;
  }

  async getData(formId, ids) {
    const processId = async id => {
      const url = `https://forms.logiforms.com/api/1.0/form/${formId}/data/${id}`;
      // eslint-disable-next-line no-await-in-loop
      const result = await this.GetJson(url);
      const { statusCode, json } = result;
      if (!json.success || statusCode !== HTTP_OK) {
        logger.error(json.error);
        return null;
      }
      return json.data.record;
    };

    return Promise.all(ids.map(id => processId(id)));
  }

  /**
   * Create data
   * @param {number|string} formId
   * @param {Object} dataObject
   * @return {Promise<Object|null>}
   */
  async createData(formId, dataObject) {
    const url = `https://forms.logiforms.com/api/1.0/form/${formId}/data/`;
    const result = await this.PostJson(url, dataObject);
    const { statusCode, json } = result;
    if (!json.success || statusCode !== HTTP_OK) {
      logger.error(json.error);
      return null;
    }
    return json.data;
  }

  /**
   * Create bulk data
   * @param {string|number} formId
   * @param {Array<Object>} dataArray
   * @return {Promise<Array<Object|null>>}
   */
  async createDataBulk(formId, dataArray) {
    return Promise.all(dataArray.map(data => this.createData(formId, data)));
  }

  /**
   * Delete record by Id
   * @param {string|number} formId
   * @param {string|number} recordId
   * @return {Promise<Object|null>}
   */
  async deleteRecord(formId, recordId) {
    const url = `https://forms.logiforms.com/api/1.0/form/${formId}/data/${recordId}`;
    const result = await this.DeleteMethod(url);
    const { statusCode, json } = result;
    if (!json.success || statusCode !== HTTP_OK) {
      logger.error(json.error);
      return null;
    }
    return json.data;
  }

  /**
   * Delete records bulk
   * @param {string|number} formId
   * @param {Array<string|number>} recordsId
   * @return {Promise<Array<Object|null>>}
   */
  async deleteRecordsBulk(formId, recordsId) {
    return Promise.all(
      recordsId.map(recordId => this.deleteRecord(formId, recordId))
    );
  }
}

module.exports = Logiforms;
