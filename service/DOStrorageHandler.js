const AWS = require('aws-sdk');
const dateformat = require('dateformat');
const logger = require('../logger')('service/DOStrorageHandler.js');

const { DO_REGION, DO_ACCESS_KEY, DO_SECRET_KEY, DO_BUCKET } = process.env;
const spacesEndpoint = new AWS.Endpoint(`${DO_REGION}.digitaloceanspaces.com`);

const getContentType = outputFilename => {
  const lowered = outputFilename.toLowerCase();
  if (lowered.indexOf('.csv') !== -1) {
    return 'application/csv';
  }
  if (lowered.indexOf('.zip') !== -1) {
    return 'application/zip';
  }
  return 'application/octet-stream';
};

// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html
class DOStrorageHandler {
  constructor() {
    this.s3 = new AWS.S3({
      endpoint: spacesEndpoint,
      accessKeyId: DO_ACCESS_KEY,
      secretAccessKey: DO_SECRET_KEY,
    });
  }

  async init() {
    this.isInit = true;
    logger.debug('Init.');
  }

  async sendFile({ data, outputFilename }) {
    logger.debug('Uploading file %s', outputFilename);
    const Key = `${dateformat(new Date(), 'yyyymmdd')}/${outputFilename}`;
    const params = {
      Body: data,
      Bucket: DO_BUCKET,
      Key,
      ACL: 'public-read',
      ContentType: getContentType(outputFilename),
      ContentDisposition: `attachment;filename=${outputFilename}`,
    };
    const dataResult = await this.s3.putObject(params).promise();
    logger.debug('Uploading finished.');
    return dataResult;
  }

  async finish() {
    this.isInit = false;
    logger.debug('Finish.');
  }

  async getList() {
    const params = {
      Bucket: DO_BUCKET,
      MaxKeys: 4000000,
    };
    logger.debug('List files');
    return this.s3.listObjectsV2(params).promise();
  }

  async getFile(params) {
    logger.debug(`Download file: ${JSON.stringify(params)}`);
    return this.s3.getObject({ Bucket: DO_BUCKET, ...params }).promise();
  }

  async copyFile(params) {
    logger.debug(`Copy file: ${JSON.stringify(params)}`);
    // eslint-disable-next-line no-param-reassign
    params.CopySource = `/${DO_BUCKET}/${params.CopySource}`;
    return this.s3.copyObject({ Bucket: DO_BUCKET, ...params }).promise();
  }

  async deleteFile(params) {
    logger.debug(`Delete file: ${JSON.stringify(params)}`);
    return this.s3.deleteObject({ Bucket: DO_BUCKET, ...params }).promise();
  }
}

module.exports = DOStrorageHandler;
