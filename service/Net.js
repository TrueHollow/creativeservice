const {
  GetRequest,
  DeleteRequest,
  GetRequestBinary,
  PostRequest,
} = require('./RequestLibProvider');
/* const {
  GetRequest,
  DeleteRequest,
  GetRequestBinary,
  PostRequest,
} = require('./NeedleLibProvider'); */
const logger = require('../logger')('service/Net.js');

const delay = async (timeout = 100) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};

class Net {
  static async GetBinary(url) {
    let data;
    do {
      try {
        logger.debug(`Performing request (${url})...`);
        // eslint-disable-next-line no-await-in-loop
        data = await GetRequestBinary(url);
        logger.debug(`...request (${url}) is finished`);
      } catch (e) {
        logger.error('(%s): %s', url, e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request (%s)', url);
      }
    } while (!data);
    return data;
  }

  static async GetJson(url) {
    let json;
    do {
      try {
        logger.debug(`Performing request (${url})...`);
        // eslint-disable-next-line no-await-in-loop
        json = await GetRequest(url);
        logger.debug(`...request (${url}) is finished`);
      } catch (e) {
        logger.error('(%s): %s', url, e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request (%s)', url);
      }
    } while (!json);
    return json;
  }

  static async PostJson(url, data) {
    let json;
    do {
      try {
        logger.debug(
          `Performing request (${url}) with data (${JSON.stringify(data)})...`
        );
        // eslint-disable-next-line no-await-in-loop
        json = await PostRequest(url, data);
        logger.debug(`...request (${url}) is finished`);
      } catch (e) {
        logger.error('(%s): %s', url, e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request (%s)', url);
      }
    } while (!json);
    return json;
  }

  /**
   * Delete method
   * @param {string} url
   * @return {Promise<Object>}
   */
  static async DeleteMethod(url) {
    let json;
    do {
      try {
        logger.debug(`Performing DELETE request (${url})...`);
        // eslint-disable-next-line no-await-in-loop
        json = await DeleteRequest(url);
        logger.debug(`...request (${url}) is finished`);
      } catch (e) {
        logger.error('(%s): %s', url, e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request (%s)', url);
      }
    } while (!json);
    return json;
  }
}

module.exports = {
  Net,
  delay,
};
