require('./service/dotEnv_proxy');
const { CronJob } = require('cron');
const dateformat = require('dateformat');
const genericPool = require('generic-pool');
const Logiforms = require('./service/Logiforms');
const IO = require('./service/IO');
const DOStrorageHandler = require('./service/DOStrorageHandler');
const config = require('./config');
const logger = require('./logger')('index.js');
const { delay } = require('./service/Net');

logger.info('Script started.');

const { LOGIFORMS_API_KEY } = process.env;
if (typeof LOGIFORMS_API_KEY !== 'string') {
  logger.error('LOGIFORMS_API_KEY is not set. Abort');
}

const factory = {
  create() {
    return new DOStrorageHandler();
  },
  async destroy(handler) {
    return handler.finish();
  },
};

const opts = {
  max: 2, // maximum size of the pool
  min: 1, // minimum size of the pool
};

const poolOfSenders = genericPool.createPool(factory, opts);
const uploadHandlers = [];
const getHandler = async () => {
  const sender = await poolOfSenders.acquire();
  uploadHandlers.push(sender);
  return sender;
};

const releaseHandler = async sender => {
  const index = uploadHandlers.indexOf(sender);
  if (index === -1) {
    logger.warn('Invalid sender!');
    return;
  }
  uploadHandlers.splice(index, 1);
  await poolOfSenders.release(sender);
};

const releaseAllHandlers = async () => {
  await Promise.all(
    uploadHandlers.map(async handler => poolOfSenders.release(handler))
  );
  uploadHandlers.splice(0, uploadHandlers.length);
};

async function processAttachments(ordersArray, logiforms, formId, Name) {
  await Promise.all(
    ordersArray.map(async record => {
      const { RecordID, INVOICE, BILLLAST } = record;
      const ftpH = await getHandler();
      const data = await logiforms.getAttachmentsFor(formId, RecordID);
      if (data) {
        await ftpH.init();
        await ftpH.sendFile({
          data,
          outputFilename: `${Name}_recepts/invoice-${INVOICE}-${BILLLAST}.zip`, // invoice-{INVOICE}-{BILLLAST}
        });
      }
      await releaseHandler(ftpH);
    })
  );
}

/**
 * Allow to filtrate items
 * @param {Array<number>} ids
 * @param {Array<Object>} items
 * @param {Array<Function>} Filtration
 * @return {Object}
 */
const itemsFiltrations = (ids, items, Filtration) => {
  const resultItems = [];
  const resultIds = [];
  items.forEach((item, index) => {
    let flag = true;
    // eslint-disable-next-line no-restricted-syntax
    for (const filterFunction of Filtration) {
      if (!flag) {
        break;
      }
      flag = filterFunction(item);
    }
    if (flag) {
      resultItems.push(item);
      resultIds.push(ids[index]);
    }
  });
  logger.debug(
    `Items before filter: ${items.length}, after: ${resultItems.length}`
  );
  return {
    ids: resultIds,
    items: resultItems,
  };
};

const IterationDelay = 1000 * 61; // 1 minute and 1 second
const processForm = async form => {
  const { Name, Attachments, Filtration } = form;
  logger.info('Start processing form: %s', Name);
  const logiforms = new Logiforms(form);
  const formId = await logiforms.getFormId();
  if (!formId) {
    logger.error('Form id not received!');
    return;
  }
  logger.info(`Getting ids for form (${formId})`);
  let ids = await logiforms.getIds(formId);
  if (ids.length === 0) {
    logger.info(`ID's count is zero. No new orders at current moment. Finish.`);
    return;
  }
  logger.info(`Getting data for form`);
  let ordersArray = await logiforms.getData(formId, ids);
  if (Array.isArray(Filtration)) {
    const filtrationResult = itemsFiltrations(ids, ordersArray, Filtration);
    ids = filtrationResult.ids;
    ordersArray = filtrationResult.items;
  }
  logger.info(`Convert orders to csv buffer`);
  const data = await IO.GetBufferResult(ordersArray);
  const uploadHandler = await getHandler();
  await uploadHandler.init();
  logger.info(`Uploading files:`);
  await uploadHandler.sendFile({
    data,
    outputFilename: `${Name}-${dateformat(new Date(), 'yyyymmdd')}.csv`,
  });
  await releaseHandler(uploadHandler);
  if (Attachments) {
    await processAttachments(ordersArray, logiforms, formId, Name);
  }
  logger.info(`Deleting processed orders`);
  // await logiforms.deleteSortedRecords(formId, ids);
  await logiforms.deleteRecordsBulk(formId, ids);
  await delay(IterationDelay);
};

const main = async () => {
  try {
    logger.info('Invoking main');
    // eslint-disable-next-line no-restricted-syntax
    for (const form of config.Logiforms.Forms) {
      // eslint-disable-next-line no-await-in-loop
      await processForm(form);
    }
  } catch (e) {
    logger.error('Unexpected error: %s', e);
  } finally {
    await releaseAllHandlers();
  }
  logger.info('main finished');
};

const schedule = process.env.CRON_SCHEDULE || config.Cron.Schedule;

const job = new CronJob(schedule, main, null, false, null, null, true);
job.start();
