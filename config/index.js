module.exports = {
  log4js: {
    appenders: {
      console: {
        type: 'stdout',
      },
    },
    categories: {
      default: {
        appenders: ['console'],
        level: 'debug',
      },
    },
  },
  Logiforms: {
    Forms: [
      {
        Name: 'Order Form 3.0',
        Past: 7776000000,
        Attachments: true,
        Filtration: [
          item => {
            return item.CASHRECEIVED;
          },
        ],
      },
      {
        Name: 'Book Orders',
        Past: 15552000000,
        Attachments: false,
      },
    ],
    Limiter: {
      // Bottleneck configuration
      // https://www.npmjs.com/package/bottleneck#constructor
      maxConcurrent: 60,
      minTime: 50,
      reservoir: 60,
      reservoirRefreshAmount: 60,
      reservoirRefreshInterval: 61 * 1000,
    },
  },
  Cron: {
    Schedule: '0 0 19 * * 1',
  },
  GoogleDrive: {
    Root_Folder: '1t2iIdI56r16eQ5kGOhkZzEy3KupoKGih',
  },
  Ftp: {
    Root_Folder: '/public_ftp/logiforms_export',
  },
};
